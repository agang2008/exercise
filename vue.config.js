const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
module.exports = {
  productionSourceMap: false,
  outputDir: 'dist',
  assetsDir: 'static',
  configureWebpack: {
    externals: {
      'vue': 'Vue',
      'vue-router': 'VueRouter',
      'vuex': 'Vuex',
      'axios': 'axios',
      'element-ui': 'ELEMENT',
      'qs': 'qs',
      'nprogress': 'NProgress',
    },
    plugins: [
      new BundleAnalyzerPlugin()
    ]
  },
  devServer: {
    proxy: {
      '/api': {
        target: 'http://songqiang.club/aheadData',
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
}
