function setSession(key, obj) {
  sessionStorage.setItem(key, JSON.stringify(obj))
}

function getSession(key) {
  return JSON.parse(sessionStorage.getItem(key))
}

export {
  setSession,
  getSession
}
