function letter(num) {
  return String.fromCharCode(65 + num)
}

function deepClone(obj) {
  return JSON.parse(JSON.stringify(obj))
}

function getTopicTag(type) {
  let topicType = {
    name: null,
    tag: null
  };
  switch (parseInt(type)) {
    case 0:
      topicType.name = '单选'
      topicType.tag = 'success'
      break;
    case 1:
      topicType.name = '多选'
      topicType.tag = 'warning'
      break;
    case 2:
      topicType.name = '简答'
      topicType.tag = ''
      break;
    case 3:
      topicType.name = '判断'
      topicType.tag = 'danger'
      break;
    case 4:
      topicType.name = '填空'
      topicType.tag = 'info'
      break;
  }
  return topicType
}

function formatTime(second) {
  let minute = parseInt(second / 60);
  second = second % 60;
  return {
    minute: minute < 10 ? '0' + minute : minute,
    second: second < 10 ? '0' + second : second
  }
}

function shuffle(arr) {
  let _arr = arr.slice();
  return _arr.sort(() => Math.random() - 0.5);
}

export {
  letter,
  deepClone,
  getTopicTag,
  formatTime,
  shuffle
}
