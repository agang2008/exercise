import axios from "axios";
import NProgress from "nprogress"
import store from "../store/store";


const {Notification} = ELEMENT;
const {stringify} = Qs;

const baseURL = process.env.NODE_ENV === 'development' ? '/api/' : 'http://songqiang.club/aheadData/'
const service = axios.create({
  baseURL,
  timeout: 6000,
})

service.interceptors.request.use(config => {
  NProgress.start()
  config.method !== 'get' && (config.data = stringify(config.data))
  const token = store.state.user && store.state.user.token;
  token && (config.headers['token'] = token)
  return config;
}, handleError)

service.interceptors.response.use(response => {
  NProgress.done()
  if (response.data.jianda)
    return response.data
  if (response.data.state === 1)
    return response.data.data;
  return response;
}, handleError)

function handleError(error) {
  NProgress.done();
  Notification.error({
    title: "错误",
    message: "网络请求错误!",
    duration: 2500
  })
  return Promise.reject(error)
}

export default service;
