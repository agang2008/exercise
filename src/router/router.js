import Vue from 'vue'

import VueRouter from 'vue-router'
import NProgress from "nprogress"

const {Message} = ELEMENT;


import store from "@/store/store"

Vue.use(VueRouter);
const router = new VueRouter({
  routes: [
    {
      path: "*",
      redirect: '/home'
    },
    {
      path: "/home",
      name: 'home',
      component: resolve => require(['../view/home/index'], resolve)
    },
    {
      path: "/login",
      name: 'login',
      component: resolve => require(['../view/login/index'], resolve)
    },
    {
      path: "/subject",
      name: 'subject',
      component: resolve => require(['../view/subject/index'], resolve),
      children: [
        {
          path: ":subId",
          name: 'questionBank',
          props: true,
          component: resolve => require(['../view/questionBank/index'], resolve),
          children: [
            {
              path: ":queBankId",
              name: 'answer',
              props: true,
              component: resolve => require(['../view/answer/index'], resolve),
            },
            {
              path: ":queBankId/grade",
              name: 'grade',
              props: true,
              component: resolve => require(['../view/answer/grade'], resolve),
            }
          ]
        },
      ]
    },
    {
      path: "/history",
      name: 'history',
      component: resolve => require(['../view/history/index'], resolve),
      children: [
        {
          path: "grade",
          name: 'historyGrade',
          component: resolve => require(['../view/answer/grade'], resolve),
        }
      ]
    }
  ],
  mode: "history"
})
router.beforeEach(async (to, from, next) => {
  NProgress.start()
  if (to.name === 'null') {
    //未开发功能
    Message.closeAll()
    Message('敬请期待!')
    NProgress.done()
    return;
  }
  const {state, commit} = store

  if (state.user === null && to.name !== 'login')
    next({name: 'login'})
  if (state.user && to.name === 'login')
    next({name: 'home'})
  next()
})

router.afterEach(((to, from) => {
  NProgress.done()
}))

export default router
