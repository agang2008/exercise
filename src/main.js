import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
import mCard from '@/components/m-card'

Vue.component(mCard.name, mCard)

import service from "./axios/axios"

Vue.prototype.baseUrl = 'http://songqiang.club/'

Vue.prototype.axios = service
import router from "@/router/router"
import store from "@/store/store"
new Vue({
  render: h => h(App),
  router,
  store
}).$mount('#app')
