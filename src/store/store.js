import Vue from 'vue'
import Vuex from 'vuex'
import login from "@/store/login"
import topic from "@/store/topic"
import rest from "@/store/rest"

Vue.use(Vuex)

const state = () => {
  return {
    ...login.state(),
    ...topic.state(),
    ...rest.state()
  }
}

const getters = {
  ...login.getters,
  ...topic.getters,
  ...rest.getters
}
const mutations = {
  ...login.mutations,
  ...topic.mutations,
  ...rest.mutations
}
const actions = {
  ...login.actions,
  ...topic.actions,
  ...rest.actions
}
const store = new Vuex.Store({
  state,
  getters,
  mutations,
  actions
})

export default store
